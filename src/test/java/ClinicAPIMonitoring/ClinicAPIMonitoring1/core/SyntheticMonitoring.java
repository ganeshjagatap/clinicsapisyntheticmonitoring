package ClinicAPIMonitoring.ClinicAPIMonitoring1.core;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;



@Listeners(TestListner.class)
public class SyntheticMonitoring {
	public static JSONObject responseJson_object=null;
	public static StringBuffer sb=null;
	
	String authToken=null;
	public static String mailUrl=null;
	String token=null;
	String username="APISYNTHETIC";
	String orgId=null;
	String clinicId=null;
	
	@BeforeClass
	public void init()
	{
		sb=new StringBuffer("Failure Cases From API !!!!!!!\n\n");  
		System.out.println("size:"+sb.length());
	}
	
	
	
	
	@Test(priority=0)
	public void login() throws Exception{
	mailUrl = Envirnonment.EMRprodENv + Constants.login2;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenarator.getEMRSignIn_Param(username, "Test@123");

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		//String x= responseJson_object.toString();
		 token=responseJson_object.getString("token");
		 clinicId=responseJson_object.getString("clinicUserId");
		 JSONArray e=responseJson_object.getJSONArray("organisation");
		JSONObject y=e.getJSONObject(0);
		orgId=y.getString("organisationId");
			//JSONObject e=	responseJson_object.getJSONObject("organisation");
			// orgId=	e.getString("organisationId");
		 System.out.println("Clinic User id "+ clinicId);
		System.out.println("token value  "+token);
		System.out.println("organistion Id  "+orgId);
		Assert.assertNotNull(token, "token is null");
		Assert.assertNotNull(clinicId, "ClinicUser ID is null");
		Assert.assertNotNull(orgId, "Organisation ID is null");
	//////
		
		
		
	}
	
	
	
	@Test(priority=1)
	public void PatientSearch() throws Exception{
	mailUrl = Envirnonment.EMRprodENv + Constants.patientSearch;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> patientSearch = ParamGenarator.getPatienSearch_Param(token,username,orgId, clinicId,"1","10","n"); 
JSONObject param=ParamGenarator.getJSONPatientSearch("", "", "s", "","Male");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, patientSearch, param);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		//String x= responseJson_object.toString();
		/* JSONArray e=responseJson_object.getJSONArray("patientList");
			JSONObject y=e.getJSONObject(0);
			String patienID=y.getString("patientId");*/
				JSONObject e=	responseJson_object.getJSONObject("patientList");
			JSONArray	patien=	e.getJSONArray("patient");
			JSONObject y=patien.getJSONObject(0);
			String patienID=y.getString("patientId");
		

			System.out.println("Patient Id  "+patienID);
			Assert.assertNotNull(patienID, "PatientID is null");
		
	}
	
	@Test(priority=2)
	public void appointmentHomeDoctor() throws Exception{
	mailUrl = Envirnonment.EMRprodENv + Constants.homeDoctor;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> homeDoctor = ParamGenarator.getHomeDoctor_Param(token,username,orgId, clinicId,"28-04-2017"); 

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, homeDoctor);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		//String x= responseJson_object.toString();
		/* JSONArray e=responseJson_object.getJSONArray("patientList");
			JSONObject y=e.getJSONObject(0);
			String patienID=y.getString("patientId");*/
				JSONObject e=	responseJson_object.getJSONObject("bookedSlots");
			JSONArray	patien=	e.getJSONArray("bookedSlot");
			JSONObject y=patien.getJSONObject(0);
			String normalBookSize=y.getString("normalBookSize");
		

			System.out.println("normalBookSize:    "+normalBookSize);
			Assert.assertNotNull(normalBookSize, "normalBookSize is null");
		
	}
	
	@Test(priority=3)
	public void logout() throws Exception{
	mailUrl = Envirnonment.EMRprodENv + Constants.logout;

	System.out.println("mailUrl "+mailUrl);
	String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
	RequestUtil requestUtil = new RequestUtil();

	// Get request for the session creation first.
	HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
	System.out.println("begin");

	List<NameValuePair> signinparams = ParamGenarator.getEMRlogout_Param("APISYNTHETIC", token);

	// Post request to the URL with the param data
	HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
	 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
	 
	//String x= responseJson_object.toString();
	String sucess=responseJson_object.getString("successMessage");
	
	
	Assert.assertEquals(sucess.trim(), "User logged out successfully");
	
	}
	
	
	// 1.2 version
	
	/*@Test(priority=4)
	public void loginVer12() throws Exception{
	mailUrl = Envirnonment.EMRprodENv + Constants.login21;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenarator.getEMRSignIn_Param(username, "1234");

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		//String x= responseJson_object.toString();
		 token=responseJson_object.getString("token");
		 clinicId=responseJson_object.getString("clinicUserId");
		 JSONArray e=responseJson_object.getJSONArray("organisation");
		JSONObject y=e.getJSONObject(0);
		orgId=y.getString("organisationId");
			//JSONObject e=	responseJson_object.getJSONObject("organisation");
			// orgId=	e.getString("organisationId");
		 System.out.println("Clinic User id "+ clinicId);
		System.out.println("token value  "+token);
		System.out.println("organistion Id  "+orgId);
		Assert.assertNotNull(token, "token is null");
		Assert.assertNotNull(clinicId, "ClinicUser ID is null");
		Assert.assertNotNull(orgId, "Organisation ID is null");
	//////
		
		
		
	}
	
	
	
	@Test(priority=5)
	public void PatientSearchVer12() throws Exception{
	mailUrl = Envirnonment.EMRprodENv + Constants.patientSearch1;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> patientSearch = ParamGenarator.getPatienSearch_Param(token,username,orgId, clinicId,"1","10","n"); 
JSONObject param=ParamGenarator.getJSONPatientSearch("", "", "s", "","Male");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, patientSearch, param);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		//String x= responseJson_object.toString();
		 JSONArray e=responseJson_object.getJSONArray("patientList");
			JSONObject y=e.getJSONObject(0);
			String patienID=y.getString("patientId");
				JSONObject e=	responseJson_object.getJSONObject("patientList");
			JSONArray	patien=	e.getJSONArray("patient");
			JSONObject y=patien.getJSONObject(0);
			String patienID=y.getString("patientId");
		

			System.out.println("Patient Id  "+patienID);
			Assert.assertNotNull(patienID, "PatientID is null");
		
	}
	
	@Test(priority=6)
	public void appointmentHomeDoctorVer12() throws Exception{
	mailUrl = Envirnonment.EMRprodENv + Constants.homeDoctor1;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> homeDoctor = ParamGenarator.getHomeDoctor_Param(token,username,orgId, clinicId,"08-12-2016"); 

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, homeDoctor);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		//String x= responseJson_object.toString();
		 JSONArray e=responseJson_object.getJSONArray("patientList");
			JSONObject y=e.getJSONObject(0);
			String patienID=y.getString("patientId");
				JSONObject e=	responseJson_object.getJSONObject("bookedSlots");
			JSONArray	patien=	e.getJSONArray("bookedSlot");
			JSONObject y=patien.getJSONObject(0);
			String normalBookSize=y.getString("normalBookSize");
		

			System.out.println("normalBookSize:    "+normalBookSize);
			Assert.assertNotNull(normalBookSize, "normalBookSize is null");
		
	}
	
	@Test(priority=7)
	public void logoutVer12() throws Exception{
	mailUrl = Envirnonment.EMRprodENv + Constants.logout1;

	System.out.println("mailUrl "+mailUrl);
	String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
	RequestUtil requestUtil = new RequestUtil();

	// Get request for the session creation first.
	HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
	System.out.println("begin");

	List<NameValuePair> signinparams = ParamGenarator.getEMRlogout_Param("asc3", token);

	// Post request to the URL with the param data
	HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
	 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
	 
	//String x= responseJson_object.toString();
	String sucess=responseJson_object.getString("successMessage");
	
	
	Assert.assertEquals(sucess.trim(), "User logged out successfully");
	
	}

	
	// 1.3 version
	
	@Test(priority=8)
	public void loginVer13() throws Exception{
	mailUrl = Envirnonment.EMRprodENv + Constants.login22;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> signinparams = ParamGenarator.getEMRSignIn_Param(username, "1234");

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		//String x= responseJson_object.toString();
		 token=responseJson_object.getString("token");
		 clinicId=responseJson_object.getString("clinicUserId");
		 JSONArray e=responseJson_object.getJSONArray("organisation");
		JSONObject y=e.getJSONObject(0);
		orgId=y.getString("organisationId");
			//JSONObject e=	responseJson_object.getJSONObject("organisation");
			// orgId=	e.getString("organisationId");
		 System.out.println("Clinic User id "+ clinicId);
		System.out.println("token value  "+token);
		System.out.println("organistion Id  "+orgId);
		Assert.assertNotNull(token, "token is null");
		Assert.assertNotNull(clinicId, "ClinicUser ID is null");
		Assert.assertNotNull(orgId, "Organisation ID is null");
	//////
		
		
		
	}
	
	
	
	@Test(priority=9)
	public void PatientSearchVer13() throws Exception{
	mailUrl = Envirnonment.EMRprodENv + Constants.patientSearch2;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> patientSearch = ParamGenarator.getPatienSearch_Param(token,username,orgId, clinicId,"1","10","n"); 
JSONObject param=ParamGenarator.getJSONPatientSearch("", "", "s", "","Male");
		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSONArrayy_Request(mailUrl, patientSearch, param);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		//String x= responseJson_object.toString();
		 JSONArray e=responseJson_object.getJSONArray("patientList");
			JSONObject y=e.getJSONObject(0);
			String patienID=y.getString("patientId");
				JSONObject e=	responseJson_object.getJSONObject("patientList");
			JSONArray	patien=	e.getJSONArray("patient");
			JSONObject y=patien.getJSONObject(0);
			String patienID=y.getString("patientId");
		

			System.out.println("Patient Id  "+patienID);
			Assert.assertNotNull(patienID, "PatientID is null");
		
	}
	
	@Test(priority=10)
	public void appointmentHomeDoctorVer13() throws Exception{
	mailUrl = Envirnonment.EMRprodENv + Constants.homeDoctor2;
	
	System.out.println("mailUrl "+mailUrl);
		String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> homeDoctor = ParamGenarator.getHomeDoctor_Param(token,username,orgId, clinicId,"08-12-2016"); 

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, homeDoctor);
		 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		 
		//String x= responseJson_object.toString();
		 JSONArray e=responseJson_object.getJSONArray("patientList");
			JSONObject y=e.getJSONObject(0);
			String patienID=y.getString("patientId");
				JSONObject e=	responseJson_object.getJSONObject("bookedSlots");
			JSONArray	patien=	e.getJSONArray("bookedSlot");
			JSONObject y=patien.getJSONObject(0);
			String normalBookSize=y.getString("normalBookSize");
		

			System.out.println("normalBookSize:    "+normalBookSize);
			Assert.assertNotNull(normalBookSize, "normalBookSize is null");
		
	}
	
	@Test(priority=11)
	public void logoutVer13() throws Exception{
	mailUrl = Envirnonment.EMRprodENv + Constants.logout2;

	System.out.println("mailUrl "+mailUrl);
	String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
	RequestUtil requestUtil = new RequestUtil();

	// Get request for the session creation first.
	HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
	System.out.println("begin");

	List<NameValuePair> signinparams = ParamGenarator.getEMRlogout_Param("asc3", token);

	// Post request to the URL with the param data
	HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
	 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
	 
	//String x= responseJson_object.toString();
	String sucess=responseJson_object.getString("successMessage");
	
	
	Assert.assertEquals(sucess.trim(), "User logged out successfully");
	
	}
	*/
	
	 @AfterClass
	    public void afterRun()
	    
	   {
		 //  System.out.println("Failed:"+sb.toString());
		if(sb.length()>31){   
			 System.out.println("Failed Mail:"+sb.toString());
		
	//	SendMailTLS.FailureMailsend("[Production API]: ",sb.toString());
		}

	   
	   }


}
