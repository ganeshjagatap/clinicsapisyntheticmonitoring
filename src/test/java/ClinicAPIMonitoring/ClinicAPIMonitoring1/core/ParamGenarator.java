package ClinicAPIMonitoring.ClinicAPIMonitoring1.core;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class ParamGenarator {
	
	public static List<NameValuePair> param = null;
	public static List<NameValuePair> getSignIn_Param(String username,String password) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("patientUsername", username));
		param.add(new BasicNameValuePair("patientPassword", password));
	
		return param;

	}
	
	
	
	public static List<NameValuePair> getEMRSignIn_Param(String username,String password) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("userLoginName", username));
		param.add(new BasicNameValuePair("password", password));
	
		return param;

	}
	
	
	public static List<NameValuePair> getEMRlogout_Param(String username,String token) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("userLoginName", username));
		param.add(new BasicNameValuePair("token", token));
	
		return param;

	}
	
	
	public static List<NameValuePair> getPatienSearch_Param(String token,String username,String orgId,String userID,String StartIndex,String interval,String hmFlag  ) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", username));
		param.add(new BasicNameValuePair("organisationId", orgId));
		param.add(new BasicNameValuePair("userId", userID));
		param.add(new BasicNameValuePair("startIndex", StartIndex));
		param.add(new BasicNameValuePair("interval", interval));
	
		param.add(new BasicNameValuePair("hmFlag", hmFlag));
		
		
		return param;

	}
	
	public static JSONObject getJSONPatientSearch( String  mobileNo, String uhid, String patientName, String dob,String gender) {

		JSONObject updatePatient = new JSONObject();

		//JSONArray patientDetailsArray = new JSONArray();
		JSONObject patientDetailsArray  = new JSONObject();
		patientDetailsArray .put("mobileNo", mobileNo);
		patientDetailsArray .put("uhid", uhid);
		patientDetailsArray .put("patientName", patientName);
		patientDetailsArray .put("dob", dob);
		patientDetailsArray .put("gender", gender);
		//accountDetailsArray.put(patientDetailsArray );

		updatePatient.put("patient", patientDetailsArray );
		System.out.println("req:"+updatePatient.toString());
		return updatePatient;

	}
	
	
	public static List<NameValuePair> getHomeDoctor_Param(String token,String username,String orgId,String userID,String appDate  ) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("token", token));
		param.add(new BasicNameValuePair("userLoginName", username));
		param.add(new BasicNameValuePair("organisationId", orgId));
		param.add(new BasicNameValuePair("userId", userID));
		param.add(new BasicNameValuePair("clinicUserId", userID));
		param.add(new BasicNameValuePair("appointmentDate", appDate));
	
		
		
		
		return param;

	}
	
	
	public static List<NameValuePair> getSignUp_Param(String username,String password,String dob,String mobile,String fname,String lname,String gender,String email_id  ) {

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("username", username));
		param.add(new BasicNameValuePair("password", password));
		param.add(new BasicNameValuePair("dob", dob));
		param.add(new BasicNameValuePair("mobile", mobile));
		param.add(new BasicNameValuePair("fname", fname));
		param.add(new BasicNameValuePair("lname", lname));
	
		param.add(new BasicNameValuePair("gender", gender));
		param.add(new BasicNameValuePair("email_id", email_id));
		
		return param;

	}
	
	
	
}
