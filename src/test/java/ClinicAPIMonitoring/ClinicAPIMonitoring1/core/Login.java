package ClinicAPIMonitoring.ClinicAPIMonitoring1.core;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;





public class Login {
	public static JSONObject responseJson_object=null;
	public static StringBuffer sb=null;
	
	String authToken=null;
	public static String mailUrl=null;
	@Test(priority =0)
	public void Login_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		//	Assert.assertEquals(httpResponse.getStatusLine().getStatusCode(), 200, "Response Status code for the Status Request is :" + httpResponse.getStatusLine().getStatusCode());

			// Getting the csrfRequestIdentifier identifier for the session and
			// verify the Response Body
		//	JSONObject responseJson = requestUtil.getJSONObjectForResponse(httpResponse);
		//	String csrfRequestIdentifier = responseJson.getString("csrfRequestIdentifier");
		//	System.out.println(csrfRequestIdentifier);
		//Assert.assertNotNull(csrfRequestIdentifier, "csrfRequestIdentifier for the Status Request is coming Null...");
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("Abhishek.puppalwar10", "123456");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 
			
			//String x= responseJson_object.toString();
			int y=responseJson_object.getInt("success");
			JSONObject e=	responseJson_object.getJSONObject("userData");
		int q=	e.getInt("patient_id");
	/*int s=responseJson_object.getInt("userData");*/
		//JSONArray x=responseJson_object.getJSONArray("userData");
//		String s=x.getString(0);
		
	//int x=responseJson_object.getInt("sucess");
			 
			//System.out.println("x value  "+x);
			System.out.println("y value  "+y);
			
			Assert.assertEquals(responseJson_object.getInt("success"), 1);
			Assert.assertNotNull(q, "Patient id is null");
			System.out.println("z value  "+q);
			// Verify the Value using assertion
			 
	}
	
	@Test(priority =1)
	public void Invalid_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("Abhishek.puppalwa", "123456");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "User doesn't exists");
			
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =2)
	public void Blank_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("", "123456");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "User doesn't exists");
			
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =3)
	public void Blank_Spaces_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("   ", "123456");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "User doesn't exists");
			
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =4)
	public void Special_Character_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("@@@@@@@@@", "123456");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "User doesn't exists");
			
			// Verify the Value using assertion
			 
	}
	
	@Test(priority =5)
	public void Max_length_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("dfdsfdsssssssssssssssssssssssssssssssssssssssssssssssssdfsfsdfdsdfsdfssdffsdffddddffdsfddsfdssdfds", "123456");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "User doesn't exists");
			
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =6)
	public void Min_length_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("d", "123456");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "User doesn't exists");
			
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =7)
	public void Username_Characters_With_Spaces_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("Abhsihek . pupalwar 10", "123456");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "User doesn't exists");
			
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =8)
	public void Alfha_Numeric_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("@33344$$$sfsfsd", "123456");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "User doesn't exists");
			
			// Verify the Value using assertion
			 
	}
	
	
	
	@Test(priority =9)
	public void Invalid_Password_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("Abhishek.puppalwar10", "adfassa");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "Invalid User Name or password");
			
			// Verify the Value using assertion
			 
	}
	
	@Test(priority =10)
	public void Blank_Password_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("Abhishek.puppalwar10", "");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "Invalid User Name or password");
			
			// Verify the Value using assertion
			 
	}
	
	
	
	
	@Test(priority =11)
	public void Special_Character_Password_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("Abhishek.puppalwar10", "$$%%%%%###&&****");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "Invalid User Name or password");
			
			// Verify the Value using assertion
			 
	}
	
	
	
	@Test(priority =12)
	public void Max_length_Password_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("Abhishek.puppalwar10", "adasadsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvdsvsdvdsvvdsvsddvs");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "Invalid User Name or password");
			
			// Verify the Value using assertion
			 
	}
	
	
	
	@Test(priority =13)
	public void Min_length_Password_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("Abhishek.puppalwar10", "a");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "Invalid User Name or password");
			
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =14)
	public void Blank_Spaces_Password_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("Abhishek.puppalwar10", "     ");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "Invalid User Name or password");
			
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =15)
	public void Password_Characters_With_Spaces_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("Abhishek.puppalwar10", "1 2 3 4 5 6");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "Invalid User Name or password");
			
			// Verify the Value using assertion
			 
	}
	
	
	
	
	@Test(priority =16)
	public void ALfha_Numeric_Password_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("Abhishek.puppalwar10","@######1232222sdfssf");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "Invalid User Name or password");
			
			// Verify the Value using assertion
			 
	}
	
	
	
	@Test(priority =17)
	public void Both_Invalid_Password_And_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("Abhishek.puppalwar","INVALID");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "User doesn't exists");
			
			// Verify the Value using assertion
			 
	}
	
	@Test(priority =18)
	public void Both_Blank_Password_And_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("","");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "User doesn't exists");
			
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =19)
	public void Invalid_Password_Given_In_Uppercase_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.logIn;
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		
			
			List<NameValuePair> signinparams = ParamGenarator.getSignIn_Param("Abhishek.puppalwar10","INVALID");

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, signinparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 	Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "User doesn't exists");
			
			// Verify the Value using assertion
			 
	}
	
	
	
	
	
}
