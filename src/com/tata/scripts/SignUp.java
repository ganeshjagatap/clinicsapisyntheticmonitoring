package com.tata.scripts;

import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.tata.core.Constants;
import com.tata.core.Envirnonment;
import com.tata.core.ParamGenarator;
import com.tata.core.RequestUtil;

public class SignUp {

	Random random=new Random();
	int randomNumber=(random.nextInt(65536)-32768);
	public static JSONObject responseJson_object=null;
	public static StringBuffer sb=null;
	
	String authToken=null;
	public static String mailUrl=null;
	@Test(priority =0)
	public void SignUp_SuccessUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.signUp;

		String username="existing+"+randomNumber+"";
		String password="vsdvdsv";
		String dob="12-08-2016";
		String mobile=""+randomNumber+"";
		String fname="ewrewr";
		String lname="sfdfdsf";
		String gender="Male";
		String email_id="fdsdsvx@gmail.com";
	
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		//	Assert.assertEquals(httpResponse.getStatusLine().getStatusCode(), 200, "Response Status code for the Status Request is :" + httpResponse.getStatusLine().getStatusCode());

			// Getting the csrfRequestIdentifier identifier for the session and
			// verify the Response Body
		//	JSONObject responseJson = requestUtil.getJSONObjectForResponse(httpResponse);
		//	String csrfRequestIdentifier = responseJson.getString("csrfRequestIdentifier");
		//	System.out.println(csrfRequestIdentifier);
		//Assert.assertNotNull(csrfRequestIdentifier, "csrfRequestIdentifier for the Status Request is coming Null...");
			
			List<NameValuePair> sigUpparams = ParamGenarator.getSignUp_Param(username, password, dob, mobile, fname, lname, gender, email_id);

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, sigUpparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 
			
			
			Assert.assertEquals(responseJson_object.getInt("success"), 1);
			Assert.assertEquals(responseJson_object.getString("message"), "Patient Account Created Successfully");
			// Verify the Value using assertion
			 
	}
	
	
	
	@Test(priority =1)
	public void SignUp_With_Existing_USername_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.signUp;
		
		String username="Abhishek.puppalwar10";
		String password="vsdvdsv";
		String dob="12-08-2016";
		String mobile="3";
		String fname="ewrewr";
		String lname="sfdfdsf";
		String gender="Male";
		String email_id="fdsdsvx@gmail.com";
	
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		//	Assert.assertEquals(httpResponse.getStatusLine().getStatusCode(), 200, "Response Status code for the Status Request is :" + httpResponse.getStatusLine().getStatusCode());

			// Getting the csrfRequestIdentifier identifier for the session and
			// verify the Response Body
		//	JSONObject responseJson = requestUtil.getJSONObjectForResponse(httpResponse);
		//	String csrfRequestIdentifier = responseJson.getString("csrfRequestIdentifier");
		//	System.out.println(csrfRequestIdentifier);
		//Assert.assertNotNull(csrfRequestIdentifier, "csrfRequestIdentifier for the Status Request is coming Null...");
			
			List<NameValuePair> sigUpparams = ParamGenarator.getSignUp_Param(username, password, dob, mobile, fname, lname, gender, email_id);

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, sigUpparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 
			
			
			Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "User already exist with the same details, Please try forgot password option with your username");
			// Verify the Value using assertion
			 
	}
	
	@Test(priority =2)
	public void SignUp_With_Existing_Mobile_Number_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.signUp;
		
		String username="existing";
		String password="vsdvdsv";
		String dob="12-08-2016";
		String mobile="123456789";
		String fname="ewrewr";
		String lname="sfdfdsf";
		String gender="Male";
		String email_id="fdsdsvx@gmail.com";
	
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			System.out.println("begin");
		//	Assert.assertEquals(httpResponse.getStatusLine().getStatusCode(), 200, "Response Status code for the Status Request is :" + httpResponse.getStatusLine().getStatusCode());

			// Getting the csrfRequestIdentifier identifier for the session and
			// verify the Response Body
		//	JSONObject responseJson = requestUtil.getJSONObjectForResponse(httpResponse);
		//	String csrfRequestIdentifier = responseJson.getString("csrfRequestIdentifier");
		//	System.out.println(csrfRequestIdentifier);
		//Assert.assertNotNull(csrfRequestIdentifier, "csrfRequestIdentifier for the Status Request is coming Null...");
			
			List<NameValuePair> sigUpparams = ParamGenarator.getSignUp_Param(username, password, dob, mobile, fname, lname, gender, email_id);

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, sigUpparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 
			
			
			Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "User already exist with the same details, Please try forgot password option with your username");
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =3)
	public void SignUp_With_Blank_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.signUp;
		
		String username="";
		String password="vsdvdsv";
		String dob="12-08-2016";
		String mobile="123456789";
		String fname="ewrewr";
		String lname="sfdfdsf";
		String gender="Male";
		String email_id="fdsdsvx@gmail.com";
	
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);


			
			List<NameValuePair> sigUpparams = ParamGenarator.getSignUp_Param(username, password, dob, mobile, fname, lname, gender, email_id);

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, sigUpparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 
			
			
			Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "User already exist with the same details, Please try forgot password option with your username");
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =4)
	public void SignUp_With_Blank_Mobile_Number_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.signUp;
		
		String username="existing+"+randomNumber+"";
		String password="vsdvdsv";
		String dob="12-08-2016";
		String mobile="";
		String fname="ewrewr";
		String lname="sfdfdsf";
		String gender="Male";
		String email_id="fdsdsvx@gmail.com";
	
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);


			
			List<NameValuePair> sigUpparams = ParamGenarator.getSignUp_Param(username, password, dob, mobile, fname, lname, gender, email_id);

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, sigUpparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 
			
			
			Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "User already exist with the same details, Please try forgot password option with your username");
			// Verify the Value using assertion
			 
	}
	
	@Test(priority =5)
	public void SignUp_With_Blank_Password_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.signUp;
		
		String username="existi+"+randomNumber+"";
		String password="";
		String dob="15-08-2016";
		String mobile=""+randomNumber+"";
		String fname="ewrewr";
		String lname="sfdfdsf";
		String gender="Male";
		String email_id="fdsdsccvx@gmail.com";
	
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);


			
			List<NameValuePair> sigUpparams = ParamGenarator.getSignUp_Param(username, password, dob, mobile, fname, lname, gender, email_id);

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, sigUpparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 
			
			
			Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "Password shoudn't be blank");
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =6)
	public void SignUp_With_Blank_DOB_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.signUp;
		
		String username="existing+"+randomNumber+"";
		String password="123456";
		String dob="";
		String mobile=""+randomNumber+"";
		String fname="ewrewr";
		String lname="sfdfdsf";
		String gender="Male";
		String email_id="fdsdsvx@gmail.com";
	
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);


			
			List<NameValuePair> sigUpparams = ParamGenarator.getSignUp_Param(username, password, dob, mobile, fname, lname, gender, email_id);

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, sigUpparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 
			
			
			Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "DOB shoudn't be blank");
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =7)
	public void SignUp_With_Future_DOB_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.signUp;
		
		String username="existing+"+randomNumber+"";
		String password="123456";
		String dob="12-08-2017";
		String mobile=""+randomNumber+"";
		String fname="ewrewr";
		String lname="sfdfdsf";
		String gender="Male";
		String email_id="fdsdsvx@gmail.com";
	
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);


			
			List<NameValuePair> sigUpparams = ParamGenarator.getSignUp_Param(username, password, dob, mobile, fname, lname, gender, email_id);

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, sigUpparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 
			
			
			Assert.assertEquals(responseJson_object.getInt("success"), 0);
			//Assert.assertEquals(responseJson_object.getString("message"), "Password shoudn't be blank");
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =8)
	public void SignUp_With_Blank_Username_With_Spaces_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.signUp;
		
		String username="       ";
		String password="123456";
		String dob="12-08-2016";
		String mobile=""+randomNumber+"";
		String fname="ewrewr";
		String lname="sfdfdsf";
		String gender="Male";
		String email_id="fdsdsvx@gmail.com";
	
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);


			
			List<NameValuePair> sigUpparams = ParamGenarator.getSignUp_Param(username, password, dob, mobile, fname, lname, gender, email_id);

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, sigUpparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 
			
			
			Assert.assertEquals(responseJson_object.getInt("success"), 0);
			//Assert.assertEquals(responseJson_object.getString("message"), "Password shoudn't be blank");
			// Verify the Value using assertion
			 
	}
	
	@Test(priority =10)
	public void SignUp_With_USername_AS_Only_Special_Character_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.signUp;
		
		String username="@@@@@%$%%$$";
		String password="123456";
		String dob="12-08-2016";
		String mobile=""+randomNumber+"";
		String fname="ewrewr";
		String lname="sfdfdsf";
		String gender="Male";
		String email_id="fdsdsvx@gmail.com";
	
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);


			
			List<NameValuePair> sigUpparams = ParamGenarator.getSignUp_Param(username, password, dob, mobile, fname, lname, gender, email_id);

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, sigUpparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 
			
			
			Assert.assertEquals(responseJson_object.getInt("success"), 0);
			//Assert.assertEquals(responseJson_object.getString("message"), "Password shoudn't be blank");
			// Verify the Value using assertion
			 
	}
	
	

	@Test(priority =11)
	public void SignUp_With_Blank_Mobile_Number_With_Spaces_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.signUp;
		
		String username="existing+"+randomNumber+"";
		String password="123456";
		String dob="12-08-2016";
		String mobile="     ";
		String fname="ewrewr";
		String lname="sfdfdsf";
		String gender="Male";
		String email_id="fdsdsvx@gmail.com";
	
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);


			
			List<NameValuePair> sigUpparams = ParamGenarator.getSignUp_Param(username, password, dob, mobile, fname, lname, gender, email_id);

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, sigUpparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 
			
			
			Assert.assertEquals(responseJson_object.getInt("success"), 0);
			//Assert.assertEquals(responseJson_object.getString("message"), "Password shoudn't be blank");
			// Verify the Value using assertion
			 
	}
	
	
	
	@Test(priority =12)
	public void SignUp_With_INVALID_Mobile_Number_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.signUp;
		
		String username="existing+"+randomNumber+"";
		String password="123456";
		String dob="12-08-2016";
		String mobile="INVALID";
		String fname="ewrewr";
		String lname="sfdfdsf";
		String gender="Male";
		String email_id="fdsdsvx@gmail.com";
	
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);


			
			List<NameValuePair> sigUpparams = ParamGenarator.getSignUp_Param(username, password, dob, mobile, fname, lname, gender, email_id);

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, sigUpparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 
			
			
			Assert.assertEquals(responseJson_object.getInt("success"), 0);
			//Assert.assertEquals(responseJson_object.getString("message"), "Password shoudn't be blank");
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =13)
	public void SignUp_With_Blank_FirstName_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.signUp;
		
		String username="existing+"+randomNumber+"";
		String password="123456";
		String dob="12-08-2016";
		String mobile=""+randomNumber+"";
		String fname="";
		String lname="sfdfdsf";
		String gender="Male";
		String email_id="fdsdsvx@gmail.com";
	
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);


			
			List<NameValuePair> sigUpparams = ParamGenarator.getSignUp_Param(username, password, dob, mobile, fname, lname, gender, email_id);

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, sigUpparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 
			
			
			Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "DOB shoudn't be blank");
			// Verify the Value using assertion
			 
	}
	
	@Test(priority =14)
	public void SignUp_With_Blank_Gender_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.signUp;
		
		String username="existing+"+randomNumber+"";
		String password="123456";
		String dob="12-08-2016";
		String mobile=""+randomNumber+"";
		String fname="sdfdsf";
		String lname="sfdfdsf";
		String gender="";
		String email_id="fdsdsvx@gmail.com";
	
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);


			
			List<NameValuePair> sigUpparams = ParamGenarator.getSignUp_Param(username, password, dob, mobile, fname, lname, gender, email_id);

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, sigUpparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 
			
			
			Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "DOB shoudn't be blank");
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =15)
	public void SignUp_ALFHA_Numeric_Mobile_Number_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.signUp;
		
		String username="existing+"+randomNumber+"";
		String password="123456";
		String dob="12-08-2016";
		String mobile="@$$#123443";
		String fname="sdfdsf";
		String lname="sfdfdsf";
		String gender="Male";
		String email_id="fdsdsvx@gmail.com";
	
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);


			
			List<NameValuePair> sigUpparams = ParamGenarator.getSignUp_Param(username, password, dob, mobile, fname, lname, gender, email_id);

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, sigUpparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 
			
			
			Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "DOB shoudn't be blank");
			// Verify the Value using assertion
			 
	}
	
	
	@Test(priority =16)
	public void SignUp_with_Max_Limit_Username_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.signUp;
		
		String username="existing+"+randomNumber+"sdfsddddddddddddddddddddddddddddfsdfdsfdsdsfdsfdsfdsfdsfdsfdsfdsfdsdsfdsdfsdfs";
		String password="123456";
		String dob="12-08-2016";
		String mobile="@$$#123443";
		String fname="sdfdsf";
		String lname="sfdfdsf";
		String gender="Male";
		String email_id="fdsdsvx@gmail.com";
	
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);


			
			List<NameValuePair> sigUpparams = ParamGenarator.getSignUp_Param(username, password, dob, mobile, fname, lname, gender, email_id);

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, sigUpparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 
			
			
			Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "DOB shoudn't be blank");
			// Verify the Value using assertion
			 
	}
	
	@Test(priority =17)
	public void USername_with_Space_FailureUsecase() throws Exception {
		mailUrl = Envirnonment.env + Constants.signUp;
		
		String username=" ist i n g+"+randomNumber+"";
		String password="123456";
		String dob="12-08-2016";
		String mobile=""+randomNumber+"";
		String fname="sdfdsf";
		String lname="sfdfdsf";
		String gender="Male";
		String email_id="fdsdsvx@gmail.com";
	
		
		System.out.println("mailUrl "+mailUrl);
			String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
			RequestUtil requestUtil = new RequestUtil();

			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);


			
			List<NameValuePair> sigUpparams = ParamGenarator.getSignUp_Param(username, password, dob, mobile, fname, lname, gender, email_id);

			// Post request to the URL with the param data
			HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(mailUrl, sigUpparams);
			 responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
			 
			
			
			Assert.assertEquals(responseJson_object.getInt("success"), 0);
			Assert.assertEquals(responseJson_object.getString("message"), "DOB shoudn't be blank");
			// Verify the Value using assertion
			 
	}
	
	
	
}
