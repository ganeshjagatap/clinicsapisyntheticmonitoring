package com.tata.scripts;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;



public class TestListner implements ITestListener {

	public void onTestStart(ITestResult result) {
		System.out.println("UseCase :" + getTestMethodName(result) + " [Start]");
	}

	public void onTestSuccess(ITestResult result) {
		System.out.println("Success list");

		// getting the size

		System.out.println("UseCase :" + getTestMethodName(result) + " [success]");
	}

	public void onTestFailure(ITestResult result) {
		System.out.println("Failure list");

		// getting the size
		String FailureMessage = result.getThrowable().getMessage();
		SyntheticMonitoring.sb.append("Request API: " + SyntheticMonitoring.mailUrl + "\nResponse: " + SyntheticMonitoring.responseJson_object + "\nFailure Message: " + FailureMessage + "\n\n");
		SendMailTLS.FailureMailsend("[Production API]: ",SyntheticMonitoring.sb.toString());
		System.out.println("UseCase :" + getTestMethodName(result) + " [Failure]");
	}

	public void onTestSkipped(ITestResult result) {
		System.out.println("UseCase :" + getTestMethodName(result) + " [Skipped]");
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		System.out.println("test failed but within success % " + getTestMethodName(result));
	}

	public void onStart(ITestContext context) {
		// System.out.println("#############on start of test " +
		// context.getName());
	}

	public void onFinish(ITestContext context) {
		// System.out.println("#############on finish of test " +
		// context.getName());
	}

	private static String getTestMethodName(ITestResult result) {
		return result.getMethod().getConstructorOrMethod().getName();
	}
}