package com.tata.core;

public class Constants {
public static String logIn="//getPatientLogin";
public static String logout="/HPPClinicsCARestApp/rest/clinicAdministration/logout";
public static String CREATE_SESSION_PATH="/rest/session/create";
public static String login2="/HPPClinicsCARestApp/rest/clinicAdministration/appLogin";
public static String patientSearch="/HPPClinicsAppointmentsRestApp/rest/clinicAppointments/patientSearch";
public static String homeDoctor="/HPPClinicsAppointmentsRestApp/rest/clinicAppointments/appointmentHomeDoctor";

public static String signUp="/getPatientSignUp";


//1.2
public static String logout1="/HPPClinicsCARestApp/rest/clinicAdministration/1.2/logout";

public static String login21="/HPPClinicsCARestApp/rest/clinicAdministration/1.2/appLogin";
public static String patientSearch1="/HPPClinicsAppointmentsRestApp/rest/clinicAppointments/1.2/patientSearch";
public static String homeDoctor1="/HPPClinicsAppointmentsRestApp/rest/clinicAppointments/1.2/appointmentHomeDoctor";

//1.3
public static String logout2="/HPPClinicsCARestApp/rest/clinicAdministration/1.3/logout";

public static String login22="/HPPClinicsCARestApp/rest/clinicAdministration/1.3/appLogin";
public static String patientSearch2="/HPPClinicsAppointmentsRestApp/rest/clinicAppointments/1.3/patientSearch";
public static String homeDoctor2="/HPPClinicsAppointmentsRestApp/rest/clinicAppointments/1.3/appointmentHomeDoctor";

}
